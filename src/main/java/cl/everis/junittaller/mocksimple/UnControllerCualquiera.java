package cl.everis.junittaller.mocksimple;

public class UnControllerCualquiera {

    UnServiceCualquiera service;

    public Boolean unMetodoCualquiera(String xmlCualquiera){

        Boolean respuesta = service.action(xmlCualquiera);

        return respuesta;

    }

}
