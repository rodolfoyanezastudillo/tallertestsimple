package cl.everis.junittaller.mocksimple;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

public class UnServiceCualquiera {

    private OtroService otroService;

    private String urlGet;

    private RestTemplate restTemplate;

    public Boolean action(String xmlCualquiera) {

        Boolean respuestaParcial = otroService.seguro(xmlCualquiera);

        return restTemplate.getForEntity(urlGet,Boolean.class).getBody() == respuestaParcial;
    }
}
