package cl.everis.junittaller.nomock;

import cl.everis.junittaller.domain.Persona;

public class NameConverter {

    public String getFullName(Persona per){

        return per.getName()+" "+per.getLastName();

    }

    public String getSirName(Persona per){

        return "mr/ms. "+ per.getName();
    }

    public String createUserName(Persona per){

        return per.getName().charAt(0)+per.getLastName().substring(0,5);

    }

}
