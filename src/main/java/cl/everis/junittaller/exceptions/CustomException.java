package cl.everis.junittaller.exceptions;

public class CustomException  extends Exception{

    String errCode;

    public CustomException(){
        super();
    }
    public CustomException(String msg){
        super(msg);
    }

    public CustomException(String errCode, String message){
        super(message);
        this.errCode = errCode;
    }

}
