package cl.everis.junittaller.exceptions;

public class AClassWithExceptions {

    ExceptionService service;

    public boolean evaluate(int num) throws CustomException {

        boolean val = service.evaluate(num);

        if(val){
            return true;
        }else{
            throw new CustomException("001","Ha habido una excepcion");
        }
    }
}
