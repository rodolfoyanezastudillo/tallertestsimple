package cl.everis.junittaller.exceptions;

public interface ExceptionService {
    boolean evaluate(int num) throws IllegalArgumentException;
}
