package cl.everis.junittaller.principios;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

public class NombreAdecuado {

  @Test
  public void login_usuarioCorrecto_retornaOk() {
  }
  @Test
  public void login_usuarioCorrecto_retornaOk2() {
  }
  @Test
  public void login_usuarioCorrecto_llamaServicioDeLogin() {
  }
  @Test
  public void login_usuarioCorrecto_escribeEnLog() {
  }
  @Test
  public void login_usuarioSoloLetras_validaOk() {
  }
  @Test
  public void login_usuarioCorrecto_servicioRetornaToken() {
  }
  
  @Test
  public void login_userIncorrecto_retornaNoOk() {
  }
  @Test
  public void login_passwordIncorrecto_retornaNoOk() {
  }
  @Test
  public void login_usuarioEnBlanco_retornaNoOk() {
  }
  @Test
  public void login_servicioNoResponde_servicioArrojaExcepcion() {
  }
  @Test
  public void login_usuarioCaracteresEspeciales_retornaNoOk() {
	  assertFalse(true);
  }
  @Test
  public void login_usuarioCaracteresEspeciales_escribeEnLog() {
	  assertFalse(true);
  }
  @Test
  public void login_passwordMayor8Digitos_retornaNoOk() {
  }
  
}
