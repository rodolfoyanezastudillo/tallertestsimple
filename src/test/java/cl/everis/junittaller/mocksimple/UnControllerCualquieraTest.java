package cl.everis.junittaller.mocksimple;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertTrue;

public class UnControllerCualquieraTest {

    @Mock
    UnServiceCualquiera service;

    @InjectMocks
    UnControllerCualquiera controllerCualquiera;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void unMetodoCualquiera_servicioOk_retornasTru(){

        Mockito.when(service.action("xml")).thenReturn(true);

        Boolean respuesta = controllerCualquiera.unMetodoCualquiera("xml");

        assertTrue(respuesta);

    }

}
