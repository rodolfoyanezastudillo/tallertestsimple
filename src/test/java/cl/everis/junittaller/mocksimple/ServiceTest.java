package cl.everis.junittaller.mocksimple;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class ServiceTest {

    @InjectMocks
    UnServiceCualquiera unServiceCualquiera;

    @Mock
    OtroService otroService;

    @Mock
    RestTemplate restTemplate;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(unServiceCualquiera,"urlGet","URL");
    }

    @Test
    public void action_llamadoCorrecto_retornaOK(){

        ResponseEntity<Boolean> responseEntity = new ResponseEntity(true, HttpStatus.MULTI_STATUS);

        when(otroService.seguro("xml")).thenReturn(true);
        when(restTemplate.getForEntity("URL",Boolean.class)).thenReturn(responseEntity);

        Boolean respuesta = unServiceCualquiera.action("xml");

        assertTrue(respuesta);

    }

}
