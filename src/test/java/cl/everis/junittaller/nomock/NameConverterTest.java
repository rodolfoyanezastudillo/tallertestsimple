package cl.everis.junittaller.nomock;


import cl.everis.junittaller.domain.Persona;
import cl.everis.junittaller.nomock.NameConverter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NameConverterTest {


    @Test
    public void getFullName_nombreYApellidoNoNull_retornaNombreApellido(){

        Persona per = new Persona();
        per.setLastName("Miranda");
        per.setName("Felipe");
        NameConverter converter = new NameConverter();

        String resultado = converter.getFullName(per);

        assertEquals("Felipe Miranda", resultado);


    }





}
